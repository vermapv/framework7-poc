import React, { useState } from 'react';
import {
 
  Page,
  LoginScreenTitle,
  List,
  ListInput,
  ListButton,
  BlockFooter,
} from 'framework7-react';

export default () => {
  const [username, setUsername] = useState('root');
  const [password, setPassword] = useState('root');
//   const signIn = () => {
//     alert(`Username: ${username}<br>Password: ${password}`, () => {
     
//     });
//   };

  return (
    <Page noToolbar noNavbar noSwipeback loginScreen>
  <LoginScreenTitle>Framework7</LoginScreenTitle>
  <List form>
    <ListInput
      label="Username"
      type="text"
      placeholder="Your username"
      value={username}
      onInput={(e) => {
        setUsername(e.target.value);
      }}
    />
    <ListInput
      label="Password"
      type="password"
      placeholder="Your password"
      value={password}
      onInput={(e) => {
        setPassword(e.target.value);
      }}
    />
  </List>
  <List>
    <ListButton  link="/list">Sign In</ListButton>
    <BlockFooter>
      Some text about login information.
      <br />
      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    </BlockFooter>
  </List>
    </Page>
  );
};