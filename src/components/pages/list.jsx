import React, { useEffect, useState } from 'react';
import Localbase from "localbase"

import {
  Page,
  Navbar,
  Subnavbar,
  Link,  
  ListButton
} from 'framework7-react';

export default () => {
const [list, setList]=useState([])
const [mode, setMode] = React.useState("online")   
const [AllUserDs, setAllUserDs] = React.useState([])
let db = new Localbase("db")

const getData= async ()=>{
    // fetch('https://jsonplaceholder.typicode.com/comments')
    // .then(response => response.json())
    // .then(json => 
    //   setList(json)
    //   )

    // new
    let data = []
    fetch(`https://jsonplaceholder.typicode.com/photos`)
    .then(response => response.json())
    .then(json => {
      data.push(json)
      if (data[0].length <= 100) {
        db.collection("AllUsers").set(data[0])
      } else {
        let length = data[0].length / 10;
        let tempData = data[0].slice(0, length)
        console.log("tempdata...", tempData)
        db.collection("AllUsers").set(tempData)
      }
      setAllUserDs(data[0])
      setMode("online")
    })
    .catch(err => {
      setMode("offline")
      db.collection("AllUsers")
        .get()
        .then(users => {
          setAllUserDs(users)
        })
    })
}

  useEffect(()=>{
    getData()
  },[])
  return (
    <Page>
      <Navbar title="Virtual List">
      <Link href="/">search functionality</Link>
        <Subnavbar inner={false}>
        </Subnavbar>
      </Navbar>

      <ListButton  link="/login">Sign In</ListButton>
      <div>
        {mode === "offline" ? (
          <div severity="error">
            You are in offline mode or some issue in your internet connection.
          </div>
        ) : null}
      </div>
      {/* 
        <div class="data-table">
          <table>
            <thead>
              <tr>
                <th >id</th>
                <th >name</th>
                <th >email</th>
              </tr>
            </thead>
            <tbody>
              {list.map((i)=>(
                <tr key={i}>
                    <td >{i.id}</td>
                    <td >{i.name}</td>
                    <td >{i.email}</td>
                </tr>
              ))} 
            </tbody>
          </table>
        </div> 
      */}

        <div class="data-table">
          <table>
            <thead>
              <tr>
                <th >id</th>
                <th >Title</th>
              </tr>
            </thead>
            <tbody>
              {AllUserDs.map((i)=>(
                <tr key={i}>
                    <td >{i.id}</td>
                    <td >{i.title}</td>
                </tr>
              ))} 
            </tbody>
          </table>
        </div>
    </Page>
      
    
  );
};