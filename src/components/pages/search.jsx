import React from 'react';
import { Page, Block, BlockTitle, Navbar, NavTitle, Link } from 'framework7-react';

export default class extends React.Component {

  
  componentDidMount() {
    this.$f7ready((f7app) => {console.log(f7app)
      const autocomplete = f7app.autocomplete.create({
        inputEl: '#searchfield',
        openIn: 'dropdown',
        source: async function (query, render) {
          const res = await fetch(`//localhost:3001/search/?value=${query}`);
          console.log(res)
          const matches = await res.json();
          render(matches);
        }
      });

      autocomplete.on('change', (selected) => {
      });
    });
   
  }  
  render() {
    
    return (
      <Page>
         <Navbar>
          <NavTitle>My App</NavTitle>
        </Navbar>
        <BlockTitle>Search</BlockTitle>
        <Block strong>
          <input id="searchfield" type="text" placeholder="Country name" />
        </Block>        
      </Page>
    );
  }
};
