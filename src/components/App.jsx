import React from 'react';
import {
  App,
  Panel,
  View,
  Statusbar,
  Popup,
  Page,
  Navbar,
  NavRight,
  Link,
  Block,
  LoginScreen,
  LoginScreenTitle,
  List,
  ListInput,
  ListButton,
  BlockFooter
} from 'framework7-react';

// import routes from '../routes';
import Search from '../components/pages/search';
import NotFoundPage from '../components/pages/NotFoundPage';
import list from '../components/pages/list';
import login from '../components/pages/login';
import users from '../components/pages/users';


export default function (props) {

  // Framework7 parameters here
  const f7params = {
    id: 'io.framework7.testapp', // App bundle ID
    name: 'Framework7', // App name
    // theme: 'auto', // Automatic theme detection
    // App routes
    routes: [
      {
        path: '/',
        component: list,
      },
      {
        path: '/login',
        component: login,
      },
      {
        path: '/list',
        component: list,
      },
      {
        path: '(.*)',
        component: NotFoundPage,
      },
    ],
  };

  return (
    <App { ...f7params }>
    {/* Current View/Router, initial page will be loaded from home.jsx component */}
    <View main url="/list" /> 
    <View url="/login" />

  </App>
  );
};
